from __future__ import print_function
import requests


class SimilarityServiceRest:
    def __init__(self, host = 'http://localhost', port = 8080):
        self.host = host
        self.port = port

    def addSpectra(self,spectra,id):
        #adds a new spectra
        requests.post("{}:{}/similarity/add".format(self.host, self.port, id), json = {'upload' : {'spectrum': spectra, 'id': id} })

    def clear(self):
        requests.post("{}:{}/similarity/clear".format(self.host, self.port, id))

    def commit(self):
        requests.put("{}:{}/similarity/commit".format(self.host, self.port, id))

    def searchSimilar(self, spectra, minimumScore):
        return requests.post("{}:{}/similarity/search".format(self.host, self.port, id), json = {'searchRequest' : {'spectrum': spectra, 'minimumScore': minimumScore} }).json()['result']

    def librarySize(self):
        return requests.get("{}:{}/similarity/librarySize".format(self.host, self.port)).json()['result']

