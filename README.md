# PyOSSA #

PyOSSA is a REST client for [OSSA OpenCL Spectral Similarity Search](https://bitbucket.org/DantesInferno/spectrumsimilarity-dante) written in Python.  It supports Python 2.7.x and Python 3.x.

### Usage ###

```
#!python

from pyossa import SimilarityServiceRest

# Create an instance of the REST client
service = SimilarityServiceRest()

# Return all matches to the given spectrum above a similarity of 900
result = service.searchSimilar("100:1 122:2 133:3", 0.9)

for match in result:
    # Each match is of the format:
    # { 'score': SIMILARITY_SCORE, 'splash': SPLASH_KEY }
```