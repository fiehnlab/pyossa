#!/usr/bin/env python

from setuptools import setup


setup(name = 'pyossa',
      version = '1.0',
      description = 'Python REST client for OSSA OpenCL Similarity Search',
      author = 'Sajjan Singh Mehta',
      author_email = 'sajjan.s.mehta@gmail.com',
      url = 'https://bitbucket.org/fiehnlab/pyossa',
      packages = ['pyossa']
)
